<?php
/*
Plugin Name: readTime
Description: gives the approximate time to read the post
Version:     1.0
Author:      Semanta Bhandari
Author URI:  https://twitter.com/semantabhandari
License:     GPL2 etc

Copyright 2016 Semanta Bhandari (email : semantabhandari@gmail.com)
(readTime) is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
  
(readTime) is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
  
You should have received a copy of the GNU General Public License
along with (readTime). If not, see (http://link to your plugin license).
*/

// counts the words in the content
function readTime_wordcount($content){
    $noWords = str_word_count(strip_tags(strtolower($content)));
    return $noWords;
}

// returns count of images both in <img> tag and wp gallery
function readTime_imagecount($content){
    $szSearchPattern = '~<img [^\>]*\ />~';
    // Run preg_match_all to grab all the images and save the results in $aPics
    preg_match_all( $szSearchPattern, $content, $aPics );
    // Check to see if we have at least 1 image
    $noPics = count($aPics[0]);

    global $post;
    $gallery = get_post_gallery_images($post);
    return count($gallery) + $noPics;
    // return $noPics;
}

// returns the content along with reading time in minutes
function readTime_readtime($content){
    $imgCount = readTime_imagecount($content);
    $wordCount = readTime_wordcount($content);
    // average word per minutes = 275WPM
    // 10 seconds for each additional images
    $minRead = ceil($wordCount/275+$imgCount*10/60);
    $newContent = '<p style ="font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-size: 24px;font-style: italic;font-variant: normal;font-weight: 400;line-height: 20px;" >'.$minRead.' min Read'.'</p>'.$content;
    return $newContent;
}

// add filter to content to add the reading time
add_filter('the_content', 'readTime_readtime');

?>