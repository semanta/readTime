=== readTime ===
Contributors: semanta
Tags: read, time, reading time, time to read, content, wordpress

=== Description ==
readTime gives the approximate time to read an article or post in wordpress. The time is calculated form the word count and image count.

Tverage reading speed = 275WPM
Time to read is calculated my dividing word count by WPM. Additional time of 10 second is added to each image.

Major features in readTime:
* number of images is taken into account
* wp gallery image count is considered

== TODO ==
* add complexity of the article into consideration
* add customiztion option
